module throosea.com/fatima

go 1.17

require (
	github.com/golang/protobuf v1.4.1
	github.com/robfig/cron v1.2.0
	google.golang.org/grpc v1.27.0
	google.golang.org/protobuf v1.23.0
	gopkg.in/yaml.v2 v2.4.0
	throosea.com/log v1.0.4
)

require (
	github.com/getsentry/sentry-go v0.10.0 // indirect
	golang.org/x/net v0.0.0-20191209160850-c0dbc17a3553 // indirect
	golang.org/x/sys v0.0.0-20190813064441-fde4db37ae7a // indirect
	golang.org/x/text v0.3.2 // indirect
	google.golang.org/genproto v0.0.0-20190819201941-24fa4b261c55 // indirect
)
